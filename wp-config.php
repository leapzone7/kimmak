<?php
define('WP_CACHE', false); // Added by WP Rocket
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lz_kimmak');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&,lm?WE: 1.PA]2|%qrya-{TP*$3&)b -D4%Wz5,-XStQhjbR6++FdmmCjs185Qr');
define('SECURE_AUTH_KEY',  'JJ;CLfDZOGfeOkLl#kR&_V4aIo!5A[P+M5rSfXyaC+>:CIuqg,ixw[^=RlQ^l$R.');
define('LOGGED_IN_KEY',    'Sj@+L_RG8_,>f>rT3gi]LL8<,~sQz>.Grc{UC5_!X]$>w9LEj(w#l+5HifR%3n|t');
define('NONCE_KEY',        '+!Lwmh*YPaYp&N&LB/9< 5ye/*A.U*11IP`IH>#+!vZWs:bKCu(Bff*p6)V)r#hJ');
define('AUTH_SALT',        'Lz!?1|n-HmY^B|HI7BY}ymhGh`0S++v3@=M][p|?{bdYc[X=mfVZH^1+Qinw7n-^');
define('SECURE_AUTH_SALT', '|Ooig,~!k_{`_?{8v{PT{}Qo/|Rr|43u+QAkI2+t_kl,#5)9-l+/R5+iME0Tm?n9');
define('LOGGED_IN_SALT',   ' hTeRQ/M>Q,p;CqlEHG3U}f0/9uC>CZKxrm).gnq6VY6bbz-y$y-#a$-|uxse(g>');
define('NONCE_SALT',       'TWk4*Z~0HGc6L<A%?g-U^5 *H xc2S=$,?D,s-)V @Wu}CN+2i-NLKV8>%@BO 4!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);
define('WP_RWL', true);


/**
 * Disable revisions post
 */
define('WP_POST_REVISIONS', false);


/**
 * Disable some javascript admin
 */
define('CONCATENATE_SCRIPTS', false);


/**
 * Increase PHP Memory
 */
define( 'WP_MEMORY_LIMIT', '64M' );


define('WP_ENV', 'development');






/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');



/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

