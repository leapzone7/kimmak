		<footer class="footer-page">
			<div class="container content">
				
				<div class="container-flex">
					<div class="footer-col footer-col-1">
						<a href="<?php echo home_url(); ?>">
							<img src="<?php echo get_template_directory_uri() . '/images/logo-white.png' ?>" alt="logo-kimmak">
						</a>
					</div>
					<div class="footer-col footer-col-2"></div>
					<div class="footer-col footer-col-3">
						<table class="table-keep-in-touch margin-bottom-10">
							<tr>
								<td class="col-icon"><i class="fa fa-map-marker"></i></td>
								<td><?php the_field('address', 'option'); ?></td>
							</tr>
							<tr>
								<?php $phone = get_field("phone", 'option'); ?>
								<td class="col-icon"><i class="fa fa-phone"></i></td>
								<td><a href="tel:<?php echo str_replace('-', '', $phone);  ?>"><?php echo $phone; ?></a></td>
							</tr>
							<tr>
								<td class="col-icon"><i class="fa fa-envelope"></i></td>
								<td><a href="mailto:<?php the_field("email", "option"); ?>"><?php the_field('email', 'option'); ?></a></td>
							</tr>
						</table>
						<?php if (get_field('use_logo', 'option')): ?>
							<?php $logo = get_field('logo', 'option'); ?>

                            <img class="isologo" src="<?php echo get_template_directory_uri() . '/images/isologo.png'; ?>" alt="isologo">

							<?php if ($logo): ?>
								<img src="<?php echo get_template_directory_uri() . '/images/safe.png' ?>" alt="bizsafe">
							<?php endif; ?>
						<?php endif; ?>
						
					</div>
				</div>
			</div>
			<div class="footer-bottom-info">
				<?php the_field('copyright_text', 'option') ?>
			</div>
		</footer>

	</div><!-- #wrapper -->

	<?php wp_footer(); ?>
</body>
</html>
