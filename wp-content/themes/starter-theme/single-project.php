<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<div class="container content-sm">
	<h1 class="single-title"><?php the_title() ?></h1>

	<?php $gallery = get_field('gallery'); ?>

	<div id="grid-container" class="cbp">
		<?php if ($gallery): ?>
			<?php foreach ($gallery as $image): ?>
				<div class="cbp-item">
					<a class="cbp-lightbox" data-title="<?php echo $image['title'] ?>" href="<?php echo $image['url'] ?>">
						<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>" width="100%"/>
					</a>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>

	<div class="w-back">
		<a title="Back to gallery" href="<?php echo home_url('/project-gallery/'); ?>"><< Back</a>
	</div>

</div>

<?php endwhile; ?>
<?php get_footer(); ?>
