<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<?php
if (is_user_logged_in()):
	$current_user_role 	= get_current_user_role();

	$role 				= get_field('role');

	if ($role == 'specific-roles'):
		$roles 	= get_field('roles');

		if(empty($roles[count($roles)-1])):
		    unset($roles[count($roles)-1]);
		endif;
	endif;
endif;
?>

<div class="w-breadcrumb">
	<div class="container">
		<div class="container-flex">
			<div class="col-flex col-flex-left">
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="col-flex col-flex-right text-right">
				<a href="<?php echo home_url(); ?>">Home</a> / <?php the_title(); ?>
			</div>
		</div>
	</div>
</div>

<div class="container content-sm">

	<h3 class="lbl-blue">PDFs download</h3>

	<div class="margin-bottom-50"></div>

	<?php if ( post_password_required() ): ?>
		
		<form action="<?php get_option('siteurl'); ?>/wp-login.php?action=postpass" method="post">
		    <p>This page is password protected. Please enter password below:</p>
		    <div class="input-group">
		    	<input class="form-control" name="post_password" id="<?php $label ?>" type="password" size="20" />
		    	<span class="input-group-btn">
		    		<button type="submit" class="btn btn-primary" type="button">Submit</button>
		    	</span>
		    </div><!-- /input-group -->
	    </form>

	<?php else: ?>

		<div class="w-pdfs">
			<ul class="list-pdfs">
			<?php 

			$files 	= get_field('files');
			if ($files):
				foreach ($files as $file):
					include locate_template('/parts/loop/content-inspection-reports.php');
				endforeach;
			endif;
			?>
			</ul>
		</div>

	<?php endif; ?>

</div>

<div id="modal-access-denied" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Access Denied</h4>
	      	</div>
			<div class="modal-body">
				<p>
				You don't have sufficient permissions to access this page!
				If you think that this is a mistake, please email to <a href="mailto:kimmak@kimmak.com.sg">kimmak@kimmak.com.sg</a> or call +65 6257 5066. Thank you.
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php endwhile; ?>
<?php get_footer(); ?>