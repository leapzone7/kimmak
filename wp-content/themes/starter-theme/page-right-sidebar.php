<?php 
/**
 * Template Name: Main content + Right sidebar
 */
?>
<?php get_header(); ?>

<div class="container content-sm">
	<div class="row">
		
		<div class="col-md-9">
			<?php
		    while(have_posts()): the_post();
		       the_content();
		    endwhile;
			?>
		</div>
		
		<div class="col-md-3">
			<?php get_sidebar(); ?>
		</div>
		
	</div>
</div><!-- /.container -->

<?php get_footer(); ?>
