(function($){

	$('#grid-container').cubeportfolio({

		defaultFilter: '*',
	    filters: '#filters-container',
	    gapHorizontal: 5,
        gapVertical: 5,
        gridAdjustment: 'responsive',
        layoutMode: 'grid',
        mediaQueries: [{
            width: 1100,
            cols: 5
        }, {
            width: 800,
            cols: 4
        }, {
            width: 480,
            cols: 3
        }, {
            width: 320,
            cols: 2
        }],
        animationType: 'quicksand',

        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

	});

    // $(document).ready(function(){
    //     $('.lightbox').lightbox({
    //         autoresize : false
    //     });
    // });
  
})(jQuery);