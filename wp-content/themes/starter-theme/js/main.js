(function($){

	$('.btn-search-top').click(function(){
		$('#searchform').toggleClass('show');
	});
	

	var wd = window.matchMedia('(min-width: 768px)');
	updateNavbar();

	$(window).resize(function(){
		updateNavbar();
	});

	function updateNavbar(){
		if (wd['matches'] == true) {
			$(".navbar-fixed-top").css("position", "fixed");
			//$('body').css('padding-top', $('.navbar').height() + 'px');
		}
		else {
			$(".navbar-fixed-top").css("position", "initial");
			//$('body').css('padding-top', 0);
		}
	}
	
	$(window).scroll(function() {
		if (wd['matches'] == true) {
			if ($(document).scrollTop() > $('.navbar').height()) {
				$('.navbar').addClass('shrink');
			} else {
				$('.navbar').removeClass('shrink');
			}
		}
	});

})(jQuery)