<?php get_header(); ?>

<div class="container">
	<?php while(have_posts()): the_post(); ?>
	<div class="row">
		<div class="col-sm-12">
			<?php the_content(); ?>
		</div>
	</div>
	<?php endwhile; ?>
	
</div><!-- /.container -->

<?php get_footer(); ?>
