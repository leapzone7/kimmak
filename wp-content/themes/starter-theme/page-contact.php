<?php 
if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
	wpcf7_enqueue_scripts();
}

if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
	wpcf7_enqueue_styles();
}
?>
<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<div class="w-breadcrumb">
	<div class="container">
		<div class="container-flex">
			<div class="col-flex col-flex-left">
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="col-flex col-flex-right text-right">
				<a href="<?php echo home_url(); ?>">Home</a> / <?php the_title(); ?>
			</div>
		</div>
	</div>
</div>

<div id="contact">
	<div class="container content-sm">

		<div class="clearfix margin-bottom-30"></div>

		<div class="row">
			<div class="col-md-8">
				<?php the_content(); ?>
			</div>
			<div class="col-md-4">
				<?php $map = get_field('map'); ?>

				<?php if( !empty($map) ): ?>
					<div class="acf-map">
						<div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"></div>
					</div>
				<?php endif; ?>

				<ul class="list-contact">
					<li class="l-phone">
						<strong>Phone:</strong>
		                <i class="glyphicon glyphicon-earphone"></i>
		                <div class="dp-inline-block">
		                	<?php $phones = get_field('phone'); ?>
		                	<?php if ($phones): ?>
								<?php foreach ($phones as $phone): ?>
									<span><?php echo $phone['name'] ?>: <?php echo $phone['phone_number'] ?></span><br>
								<?php endforeach; ?>
		                	<?php endif; ?>
		                </div>
		                
					</li>
					<li class="l-email">
						<strong>Email:</strong>
		                <i class="fa fa-envelope-o"></i>
		                <span><a class="mailto" href="mailto:<?php echo get_field('email', 'option') ?>"><?php the_field('email', 'option') ?></a></span>
					</li>
					<li class="l-address">
						<strong>Address:</strong>
						<i class="fa fa-map-marker"></i>
						<?php the_field('address', 'option') ?>
					</li>
				</ul>

			</div>
		</div>

		
	</div>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>