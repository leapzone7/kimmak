<?php
require_once locate_template('/functions/cleanup.php');
require_once locate_template('/functions/setup.php');
require_once locate_template('/functions/enqueues.php');
require_once locate_template('/functions/navbar.php');
require_once locate_template('/functions/widgets.php');
require_once locate_template('/functions/search.php');
require_once locate_template('/functions/cpt.php');
require_once locate_template('/functions/tax.php');
require_once locate_template('/functions/ajax.php');

include_once locate_template('/functions/ted-functions.php');

/*========== Your custom functions below here ==========*/


add_filter('protected_title_format', 'blank');
function blank($title) {
    return '%s';
}

?>
