<?php 
/**
 * Template Name: Left sidebar + Main content
 */
?>
<?php get_header(); ?>

<div class="container content-sm">
	<div class="row">

		<div class="col-md-3">
			
		</div>
		
		<div class="col-md-9">
			<?php
		    while(have_posts()): the_post();
		       the_content();
		    endwhile;
			?>
		</div>
		
	</div>
</div><!-- /.container -->

<?php get_footer(); ?>
