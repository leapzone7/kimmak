<?php get_header(); ?>
<?php while(have_posts()): the_post();?>
	
<div class="w-breadcrumb">
	<div class="container">
		<div class="container-flex">
			<div class="col-flex col-flex-left">
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="col-flex col-flex-right text-right">
				<a href="<?php echo home_url(); ?>">Home</a> / <?php the_title(); ?>
			</div>
		</div>
	</div>
</div>

<div class="container content-sm">
	<?php the_content(); ?>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
