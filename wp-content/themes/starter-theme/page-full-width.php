<?php
/*
Template Name: Full Width Page
*/
?>

<?php get_header(); ?>

<div class="container-fluid content-sm">
		
	<?php
    while(have_posts()): the_post();
       the_content();
    endwhile;
	?>
	
</div><!-- /.container -->

<?php get_footer(); ?>
