<?php get_header(); ?>

<?php putRevSlider( 'home' ); ?>

<?php while(have_posts()) : the_post(); ?>

<?php get_template_part('/parts/section/home', 'our-services'); ?>

<?php get_template_part('/parts/section/home', 'our-clients'); ?>

<?php get_template_part('/parts/section/home', 'inspection-reports'); ?>

<?php endwhile; ?>

<?php get_footer(); ?>