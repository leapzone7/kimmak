<?php get_header(); ?>
	
<div class="w-breadcrumb">
	<div class="container">
		<div class="container-flex">
			<div class="col-flex col-flex-left">
				<h1>Inspection Reports</h1>
			</div>
			<div class="col-flex col-flex-right text-right">
				<a href="<?php echo home_url(); ?>">Home</a> / Inspection Reports
			</div>
		</div>
	</div>
</div>

<div class="container content-sm">
	<h3 class="lbl-blue">Folders</h3>

	<div class="margin-bottom-50"></div>
	
	<div class="w-pdfs">
		<ul class="list-pdfs">

			<?php
			while(have_posts()) : the_post();
				get_template_part('/parts/loop/content', 'folder');
			endwhile;
			?>
			
		</ul>
	</div>
</div>

<section id="inspection-reports" class="bg-red">
	<div class="container">

		<div class="container-flex no-margin">
			<div class="inspection-reports-left">
				<h3>Comprehensive boiler inspection inclusive of:</h3>
				<div class="line-white"></div>
				<ul class="list-inspection-reports">
					<li>External components </li>
					<li>Gas side together with tube thickness </li>
					<li>Water/steam side together with fibre optic tube internal analysis </li>
					<li>Assessment of the steam plant and water treatment </li>
				</ul>
			</div>
			<div class="inspection-reports-right">
				<img class="img-responsive" src="<?php echo get_template_directory_uri() . '/images/inspection-reports.jpg' ?>" alt="">
			</div>
		</div>

	</div>
</section>

<?php get_footer(); ?>
