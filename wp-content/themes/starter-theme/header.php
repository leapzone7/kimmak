<!DOCTYPE html>
<html>
<head>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Required style -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/fonts.css' ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/animate.min.css' ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/bootstrap.min.css' ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/nprogress.css' ?>">
    
     <!-- Custom style -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/base.css' ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/core.css' ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/responsive.css' ?>">
    
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
    
    <?php $gg = get_field('google_analytics', 'option'); ?>
    <?php if ($gg): ?>
    <!-- Start: Google analytics -->
    <script>
        <?php echo $gg; ?>
    </script>
    <!-- End: Google analytics -->
    <?php endif; ?>

    <!--[if lt IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!--[if lt IE 10]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="wrapper">
    <header class="header-page">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">

            <div class="info-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-info-left">
                            <div class="left-info">
                                <h1>Kimmak Marine & Offshore Engineering Pte Ltd</h1>
                            </div>
                        </div>
                        <div class="col-md-8 col-info-right text-right">
                            <div class="right-info phone-top">
                                <?php $phone = '';//get_field("phone", 'option'); ?>
                                <i class="glyphicon glyphicon-earphone"></i>
                                <span>Phone: <?php echo $phone; ?></span>
                            </div>
                            <div class="right-info email-top">
                                <i class="fa fa-envelope"></i>
                                <span>
                                Email: <a class="mailto" href="mailto:<?php echo get_field('email', 'option') ?>"><?php the_field("email", "option"); ?></a>
                                </span>
                            </div>
                            <?php if (!is_user_logged_in()): ?>
                            <a href="#" class="popup-login btn btn-organge">Login / Sign up</a>
                            <?php else: ?>
                            <a href="#" class="popup-login btn btn-organge">View Profile</a>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".header-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo home_url('/'); ?>">
                        <img class="img-responsive" src="<?php echo get_template_directory_uri() . '/images/logo.png' ?>" alt="kimmak logo">
                    </a>
                </div><!-- /.navbar-header -->

                <div class="collapse navbar-collapse header-nav">    

                    <button title="Search" class="btn-search-top"><i class="fa fa-search"></i></button>

                    <form role="search" method="get" class="animated fadeInUp" id="searchform" action="<?php echo home_url(); ?>">
                        <input type="text" name="s" class="form-control input-search-top" placeholder="Type and hit enter..." required>
                    </form>

                    <?php               
                    $args = array(
                        'theme_location'    => 'header-nav',
                        'depth'             => 0,
                        'container'         => false,
                        'fallback_cb'       => false,
                        'menu_class'        => 'nav navbar-nav pull-right',
                        'walker'            => new BootstrapNavMenuWalker()
                    );
                    wp_nav_menu($args);
                    ?>
                    
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
    </header>
    


