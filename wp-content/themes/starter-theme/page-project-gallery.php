<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>
<?php $ajax_nonce = wp_create_nonce( "kimmak_n" ); ?>

<input type="hidden" id="kimmak_n" value="<?php echo $ajax_nonce; ?>"></input>

<!-- Loader -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/loader.css' ?>">
<div class="cssload-container">
	<div class="cssload-whirlpool"></div>
</div>
<!-- End Loader -->


<div class="w-breadcrumb">
	<div class="container">
		<div class="container-flex">
			<div class="col-flex col-flex-left">
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="col-flex col-flex-right text-right">
				<a href="<?php echo home_url(); ?>">Home</a> / Project Gallery
			</div>
		</div>
	</div>
</div>

<?php 
$args = array(
	'post_type'			=> 'project',
	'posts_per_page' 	=> -1,
	'post_status'		=> 'publish',
	'order'				=> 'ASC'
);

$projects = new WP_Query($args);
?>

<?php if ($projects->have_posts()): ?>
<div class="container content-sm">

	<div id="grid-container" class="cbp">
		<?php while ($projects->have_posts()): $projects->the_post(); ?>
			<?php $gallery_images = get_field('gallery'); ?>

    		<?php if ($gallery_images): ?>
    			<?php foreach ($gallery_images as $image): ?>
				<div class="cbp-item">
					<a data-title="<?php echo $image['title'] ?>" href="<?php the_permalink(); ?>">
			            <img src="<?php echo $image['sizes']['thumbnail'] ?>" alt="<?php echo $image['title'] ?>" width="100%"/>
			        </a>
                    <a href="<?php the_permalink(); ?>" title="<?php echo $image['title'] ?>">
                        <h3><?php the_title(); ?></h3>
                    </a>
			    </div>
                <?php break; ?>
    			<?php endforeach; ?>
    		<?php endif; ?>

		<?php endwhile; ?>
	</div>
</div>
<?php endif; ?>

<?php wp_reset_postdata(); ?>
<?php endwhile; ?>
<?php get_footer(); ?>