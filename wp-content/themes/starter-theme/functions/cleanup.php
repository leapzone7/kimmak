<?php


/*
Show less info to users on failed login for security.
(Will not let a valid username be known.)
*/
function show_less_login_info() { 
    return "<strong>ERROR</strong>:  Wrong username or password!"; 
}
add_filter( 'login_errors', 'show_less_login_info' );



/*==========  Hide version wordpress  ==========*/
function no_generator()  { 
    return ''; 
}
add_filter( 'the_generator', 'no_generator' );

function remove_version_footer() {
    remove_filter( 'update_footer', 'core_update_footer' ); 
}
add_action( 'admin_menu', 'remove_version_footer' );



/*==========  Footer text admin  ==========*/
function change_text_footer_admin () {
	echo "© 2016 Kimmak";
}
add_filter('admin_footer_text', 'change_text_footer_admin');



/*========== Remove widget dashboard wordpress  ==========*/
function remove_dashboard_widget() {
 	remove_meta_box( 'dashboard_primary', 'dashboard', 'side');
 	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
} 
add_action('wp_dashboard_setup', 'remove_dashboard_widget' );



/*==========  Remove icon on top bar  ==========*/
function icon_admin_bar_remove() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('itsec_admin_bar_menu');
    $wp_admin_bar->remove_menu('updates');
}
add_action('wp_before_admin_bar_render', 'icon_admin_bar_remove', 0);



/*==========  CSS admin  ==========*/
function css_admin() {

    $user = wp_get_current_user();
    if ( !in_array( 'administrator', (array) $user->roles ) ) {
        echo '<style type="text/css">';

        echo '#wp-version-message { display:none; }';
        echo '#acf-field-group-wrap > div > div.acf-column-2 > .acf-box { display:none; }';

        echo '#viewWrapper .title_line, #viewWrapper .title_line.nobgnopd, .valid_big_padding, #eg-newsletter-wrapper, .valid_big_padding_2, .tp-plugin-version { display:none; }';
        echo '</style>';
    }
}
add_action('admin_head', 'css_admin');



/*==========  CSS login  ==========*/
function iadmin_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/admin/logo-iadmin.png);
            width: 100%;
            background-size: inherit;
            margin-bottom: 0;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'iadmin_logo' );



/*==========  Remove forgot password  ==========*/
function remove_lostpassword_text ( $text ) {
    if ($text == 'Lost your password?'){$text = '';}
        return $text;
    }
add_filter( 'gettext', 'remove_lostpassword_text' );



/*==========  Remove link logo wordpress  ==========*/
function blog_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'blog_logo_url' );


/**
 * Remove Rev Slider Metabox
 */
if ( is_admin() ) {

    function remove_revolution_slider_meta_boxes() {
        remove_meta_box( 'mymetabox_revslider_0', 'page', 'normal' );
        remove_meta_box( 'mymetabox_revslider_0', 'post', 'normal' );
        remove_meta_box( 'mymetabox_revslider_0', 'inspection-reports', 'normal' );
        remove_meta_box( 'mymetabox_revslider_0', 'project', 'normal' );
    }

    add_action( 'do_meta_boxes', 'remove_revolution_slider_meta_boxes' );
    
}
?>
