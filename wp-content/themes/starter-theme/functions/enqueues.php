<?php  

function is_local(){
	if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1'):
		return true;
	elseif ($_SERVER['REMOTE_ADDR'] == '::1'):
		return true;
	endif;

	return false;
}

/**
 * Enqueue js
 */
function js_enqueue() {

	if (!is_local()):
	    wp_deregister_script( 'jquery' );
	    wp_register_script( 'jquery', ( 'http://code.jquery.com/jquery-1.11.3.min.js' ), false, null, true );
	    wp_enqueue_script( 'jquery' );
	endif;

	$rootjs 			= get_template_directory_uri() . '/js/';
	$root_lib_js 		= get_template_directory_uri() . '/js/libs/';

	wp_enqueue_script( 'bootstrap', $root_lib_js . 'bootstrap.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'component-boostrap', $root_lib_js . 'component-boostrap.js', array('bootstrap'), null, true );
	wp_enqueue_script( 'smooth-wheel', $root_lib_js . 'smooth-wheel.min.js', array('jquery'), null, true );

	if (is_home() || is_front_page()):
		wp_enqueue_style( 'cubeportfolio', get_template_directory_uri() . '/css/cubeportfolio.min.css', array(), '3.7.2' );
		wp_enqueue_script( 'cubeportfolio', $root_lib_js . 'jquery.cubeportfolio.min.js', array( 'jquery' ), '3.7.2', true );

		// wp_enqueue_style( 'lightbox', $root_lib_js . '/lightbox/themes/default/jquery.lightbox.css' );
		// wp_enqueue_script( 'lightbox', $root_lib_js . '/lightbox/jquery.lightbox.min.js', array( 'jquery'), null, true );

		wp_enqueue_script( 'init-home', $rootjs . 'init-home.js', array( 'jquery' ), null, true );

	elseif (is_page('project-gallery')):
		wp_enqueue_style( 'cubeportfolio', get_template_directory_uri() . '/css/cubeportfolio.min.css', array(), '3.7.2' );
		wp_enqueue_script( 'cubeportfolio', $root_lib_js . 'jquery.cubeportfolio.min.js', array( 'jquery' ), '3.7.2', true );

		wp_enqueue_script( 'init-project-gallery', $rootjs . 'init-project-gallery.js', array( 'jquery' , 'cubeportfolio' ), null, true );

	elseif (is_page('contact')):
		wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', array(), null, true );
		wp_enqueue_script( 'init-contact', $rootjs . 'init-contact.js', false, null, true );

	elseif (is_single() && is_singular('project')):
		wp_enqueue_style( 'cubeportfolio', get_template_directory_uri() . '/css/cubeportfolio.min.css', array(), '3.7.2' );
		wp_enqueue_script( 'cubeportfolio', $root_lib_js . 'jquery.cubeportfolio.min.js', array( 'jquery' ), '3.7.2', true );

		wp_enqueue_script( 'init-project-gallery', $rootjs . 'init-project-gallery.js', array( 'jquery' , 'cubeportfolio' ), null, true );
	endif;
	
    // AJAX JS
	$direct = get_template_directory_uri() . '/js/main.js';

	$localize = array(
		'ajaxurl' => admin_url( 'admin-ajax.php' )
	);

	wp_register_script('main', $direct, false, null, true);
	wp_enqueue_script('main');
	wp_localize_script('main', 'TEE', $localize);

}
add_action( 'wp_enqueue_scripts', 'js_enqueue' );


add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );