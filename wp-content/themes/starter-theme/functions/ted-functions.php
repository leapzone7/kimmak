<?php

/*==========  Excerpt limit ==========*/
/**
 * @param  int 		$limit 	[Số kí tự đưa vào]
 * @return string        	[Mô tả ngắn]

 */
if (!function_exists('excerpt')):
	function excerpt($limit) {
	     $excerpt = explode(' ', get_the_excerpt(), $limit);
	     if (count($excerpt)>=$limit) {
	          array_pop($excerpt);
	          $excerpt = implode(" ",$excerpt).'...';
	     } else {
	          $excerpt = implode(" ",$excerpt);
	     }
	     $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	     return $excerpt;
	}
endif;

/*==========  Get img size  ==========*/
/**
 * @param 	array or string 	$type 	['thumbail' or array(150,150)]
 * @return 	array  
 
 */
if (!function_exists('get_img_pack')):
	function get_img_pack($type) {

	    if (has_post_thumbnail()) :
			$thumbnail_id             		= get_post_thumbnail_id();
			$alt                          	= get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
			$array_image_url              	= wp_get_attachment_image_src($thumbnail_id, $type);
			$image_url                    	= $array_image_url[0];

			if (!$alt) : 
				$args = array(
					'p' 		=> $thumbnail_id,
					'post_type' => 'attachment'
				);

				$image 		= get_posts($args);
				$img_title 	= $image[0]->post_title;
				$alt        = $img_title;
			endif;		

	    else:
	    	$alt                          	= get_the_title();
	    	$image_url                    	= plugins_url( 'img/no-image.png', __FILE__ );
	    endif;

	    return array(
	    	'url' => $image_url,
	    	'alt' => $alt,
	    );
	}
endif;

/*==========  Get sticky posts  ==========*/
/**
 * @param 	int  	$number		[Number post you want to get]
 * @param 	string 	$oderby 	[order by DESC or ASC]
 * @return 	object 				[object post]

 */
if (!function_exists('get_sticky_posts')):
	function get_sticky_posts($number = 4, $orderby = 'DESC') {
		$args = array(
			'posts_per_page' => $number,
			'post__in'  => get_option( 'sticky_posts' ),
			'ignore_sticky_posts' => 1,
			'orderby' => $orderby
		);

		$news = new WP_Query($args);
		return $news;
	}
endif;

/*==========  Get related posts  ==========*/
/**
 * @param  array 	$categories 	[Array categories of post]
 * @param  int 		$number 		[Number post you want to get] 	
 * @return object           		[object posts]

 */
if (!function_exists('get_related_posts')):
	function get_related_posts($categories, $number = 10, $orderby = 'rand') {
	    $category_ids = array();
	    foreach($categories as $cat)
	        $category_ids[] = $cat->term_id;

		global $post;

		$args = array(
	        'category__in'          => $category_ids,
	        'post__not_in'          => array($post->ID),
	        'posts_per_page'     	=> $number,
	        'ignore_sticky_posts '  => 1
	    );

	    $posts = new WP_query($args);

	    return $posts;
	}
endif;



function get_current_user_role() {
	global $wp_roles;
	$current_user = wp_get_current_user();
	$roles = $current_user->roles;
	$role = array_shift($roles);
	return $role;
}