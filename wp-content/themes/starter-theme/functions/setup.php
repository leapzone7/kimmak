<?php

/**
 * Setup thumbnails size
 */
function tedmate_setup_thumbnails() {
    add_theme_support('post-thumbnails');

    update_option('thumbnail_size_w', 300);
    update_option('thumbnail_size_h', 300);

    update_option('medium_size_w', 400);
    update_option('medium_size_h', 400);

    update_option('large_size_w', 970);
}
add_action('init', 'tedmate_setup_thumbnails');


/**
 * Options page admin
 */
if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Page settings',
        'menu_title'    => 'Page settings',
        'menu_slug'     => 'ted-options-page',
        'capability'    => 'edit_posts',
        'redirect'      => true
    ));
    
    acf_add_options_sub_page(array(
        'page_title'    => 'Contact Settings',
        'menu_title'    => 'Contact',
        'parent_slug'   => 'ted-options-page',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Footer',
        'menu_title'    => 'Footer',
        'parent_slug'   => 'ted-options-page',
    ));
    
    acf_add_options_sub_page(array(
        'page_title'    => 'Others',
        'menu_title'    => 'Others',
        'parent_slug'   => 'ted-options-page',
    ));


}




/**
 * Removing Default Image Link in WordPress
 */
function wpb_imagelink_setup() {
    $image_set = get_option( 'image_default_link_type' );
    
    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}
add_action('admin_init', 'wpb_imagelink_setup', 10);




// Readmore excerpt
function new_excerpt_more($more) {
    global $post;
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');



/**
 * Remove roles
 */
remove_role( 'editor');
remove_role( 'author');
remove_role( 'contributor');