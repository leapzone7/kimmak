<?php
function ajax_function(){

	$check_security = check_ajax_referer( 'kimmak_n', 'n' );

	if ($check_security):
		$method = $_POST['method'];
		$result = '';

		switch ($method) {

			case 'get_project_gallery':
				$result = get_project_gallery($_POST['id']);
				break;

			default:
				# code...
				break;
		}
	else:
		// Wrong nonce
		echo '-1';
	endif;

	echo $result;

	exit;
}
add_action( 'wp_ajax_ajax_function', 'ajax_function' );
add_action( 'wp_ajax_nopriv_ajax_function', 'ajax_function' );



/**
 * [get_project_gallery]
 */
function get_project_gallery($post_id){

	$gallery_images = get_field('gallery', $post_id);

	$array = array();

	if ($gallery_images):
		foreach ($gallery_images as $image):

			$arr = array(
				'href'  => $image['url'],
				'title' => $image['title']
			);
			array_push($array, $arr);
		endforeach;
	endif;

	return json_encode($array);
}