<?php 
$args = array(
	'post_type'			=> 'project',
	'posts_per_page' 	=> -1,
	'post_status'		=> 'publish',
	'order'				=> 'ASC'
);

$projects = new WP_Query($args);
?>

<?php if ($projects->have_posts()): ?>
<section id="projects-gallery" class="bg-grey">
	<div class="container content-sm">

		<div class="text-center">
			<h2 class="title-section">Projects Gallery</h2>
			<div class="line-title"></div>
		</div>

		<div class="margin-bottom-20"></div>

		<div id="filters-container">
		    <div data-filter="*" class="cbp-filter-item cbp-filter-item-active">All</div>
		    <?php while ($projects->have_posts()): $projects->the_post(); ?>
			    <div data-filter=".<?php echo $post->post_name; ?>" class="cbp-filter-item"><?php the_title(); ?></div>
			<?php endwhile; ?>
		</div>

		<div id="grid-container" class="cbp">
			<?php while ($projects->have_posts()): $projects->the_post(); ?>
				<?php $gallery_images = get_field('gallery'); ?>

	    		<?php if ($gallery_images): ?>
	    			<?php foreach ($gallery_images as $image): ?>
					<div class="cbp-item <?php echo $post->post_name; ?>">
						<a class="cbp-lightbox" data-title="<?php echo $image['title'] ?>" href="<?php echo $image['url'] ?>">
				            <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>" width="100%"/>
				        </a>
				    </div>
	    			<?php endforeach; ?>
	    		<?php endif; ?>

			<?php endwhile; ?>
		</div>
		
		<!--
		<div class="text-center w-btn-more-project">
			<a href="<?php echo home_url('/project-gallery/') ?>" class="btn btn-organge">More</a>
		</div>
		-->
	</div>
</section>

<?php wp_reset_postdata(); ?>

<?php endif; ?>
