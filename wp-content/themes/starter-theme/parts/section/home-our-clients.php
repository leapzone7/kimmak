<?php $clients = get_field('our_clients'); ?>
<?php $why_choose_us = get_field('why_choose_us'); ?>

<section id="our-clients">
	<div class="container content-sm">
			
		<div class="col-why-choose-us ">

			<?php if ($why_choose_us): ?>
				<div class="w-title text-center">
					<h3 class="title-section">Why Choose Us</h3>
					<div class="line-title"></div>
				</div>

				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6">
						<div class="panel-group accordion-why-choose-us" id="accordion" role="tablist" aria-multiselectable="true">
							<?php
							$i = 1;
							foreach ($why_choose_us as $row):
								include locate_template('/parts/loop/content-why-choose-us.php');
								$i++;
							endforeach;
							?>
						</div>
					</div>
					<div class="col-md-3"></div>
				</div>

				
			<?php endif; ?>

		</div>

	</div>
</section>