<section id="inspection-reports" class="bg-red">
	<div class="container">

		<div class="container-flex no-margin">
			<div class="inspection-reports-left">
				<h3>Comprehensive boiler inspection inclusive of:</h3>
				<div class="line-white"></div>
				<ul class="list-inspection-reports">
					<li>External components </li>
					<li>Gas side together with tube thickness </li>
					<li>Water/steam side together with fibre optic tube internal analysis </li>
					<li>Assessment of the steam plant and water treatment </li>
				</ul>
			</div>
			<div class="inspection-reports-right">
				<img class="img-responsive" src="<?php echo get_template_directory_uri() . '/images/inspection-reports.jpg' ?>" alt="">
			</div>
		</div>

	</div>
</section>