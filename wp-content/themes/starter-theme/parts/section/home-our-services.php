<section id="our-services">
	<div class="container content-sm">
		<h2 class="title-section">Our Services</h2>

		<div class="margin-bottom-20"></div>

		<div class="row">
			<div class="col-md-6">
				<ul class="list-unstyled list-our-services">
					<?php 

					$list_services = get_field('our_services');

					if ($list_services):
						foreach ($list_services as $service):
							?>
							<li><?php echo $service['text']; ?></li>
							<?php
						endforeach;
					endif; 

					?>
				</ul>
			</div>
			<div class="col-md-6">
				<div class="container-flex">
					<div class="our-service-left">
						<img class="img-responsive" src="<?php echo get_template_directory_uri() . '/images/our-service-1.jpg' ?>" alt="">
						<div class="margin-bottom-30"></div>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() . '/images/our-service-2.jpg' ?>" alt="">
					</div>
					<div class="our-service-right">
						<img class="img-responsive" src="<?php echo get_template_directory_uri() . '/images/our-service-3.jpg' ?>" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>