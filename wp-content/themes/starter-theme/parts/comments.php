<?php

// Do not delete this section
if (isset($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])){
    die ('Vui lòng không tải trang này trực tiếp. Cảm ơn!'); 
}
if ( post_password_required() ) { ?>
<div class="alert alert-warning">
    Bài viết này đã khóa. Nhập mật khẩu để bình luận.
</div>
<?php
return; 
}
// End do not delete section

if (have_comments()) : ?>

    <h3><?php _e('Feedback', 'bst'); ?></h3>
    <p class="text-muted" style="margin-bottom: 20px;">
       <i class="glyphicon glyphicon-comment"></i>&nbsp; Số bình luận: <?php comments_number('Không', '1', '%'); ?>
    </p>

    <ol class="commentlist">
      <?php wp_list_comments('type=comment&callback=bst_comment');?>
    </ol>

    <ul class="pagination">
      <li class="older"><?php previous_comments_link() ?></li>
      <li class="newer"><?php next_comments_link() ?></li>
    </ul>

    <?php
else :
    if (comments_open()) :
        echo "<p class='alert alert-info'>Hãy là người bình luận đầu tiên.</p>";
    else :
        echo "<p class='alert alert-warning'>Bình luận đã đóng ở bài viết này.</p>";
    endif;

endif;
?>

<?php if (comments_open()) : ?>
    <section id="respond">
        <h3><?php comment_form_title('Bình luận của bạn', __('Responses to %s', 'bst')); ?></h3>
        <p><?php cancel_comment_reply_link(); ?></p>

        <?php if (get_option('comment_registration') && !is_user_logged_in()) : ?>
            <p><?php printf('Bạn phải <a href="%s">đăng nhập</a> để gửi bình luận.', wp_login_url(get_permalink())); ?></p>
        <?php else : ?>
            <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
                <?php if (is_user_logged_in()) : ?>
                    <p>
                        <?php printf('Đang đăng nhập <a href="%s/wp-admin/profile.php">%s</a>.', get_option('siteurl'), $user_identity); ?>
                        <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Đăng xuất"> Đăng xuất <i class="glyphicon glyphicon-arrow-right"></i></a>
                    </p>

                <?php else : ?>
                    <div class="form-group">
                        <label for="author">
                            Tên của bạn:  
                            <?php 
                                if ($req) echo '<span class="text-muted">(*)</span>'; 
                            ?>
                        </label>
                        <input type="text" class="form-control" name="author" id="author" placeholder="Tên bạn" value="<?php echo esc_attr($comment_author); ?>" <?php if ($req) echo 'required'; ?>>
                    </div>

                    <div class="form-group">
                        <label for="email">
                            Địa chỉ Email: 
                            <?php
                                if ($req) echo '<span class="text-muted">(*)</span>';
                            ?>
                        </label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Địa chỉ email" value="<?php echo esc_attr($comment_author_email); ?>" <?php if ($req) echo 'required'; ?>>
                    </div>

                    <div class="form-group">
                        <label for="url">
                            Website của bạn:
                        </label>
                        <input type="url" class="form-control" name="url" id="url" placeholder="Website của bạn" value="<?php echo esc_attr($comment_author_url); ?>">
                    </div>
                <?php endif; ?>

                <div class="form-group">
                    <label for="comment">Bình luận của bạn:</label>
                    <textarea name="comment" class="form-control" id="comment" placeholder="Nội dung" rows="8" required></textarea>
                </div>

                <p><input name="submit" class="btn btn-default" type="submit" id="submit" value="Gửi bình luận"></p>
                
                <?php comment_id_fields(); ?>
                <?php do_action('comment_form', $post->ID); ?>
            </form>
        <?php endif; ?>
    </section>
<?php endif; ?>
