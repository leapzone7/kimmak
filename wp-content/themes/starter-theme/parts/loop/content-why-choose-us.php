<div class="panel panel-default">
	<div class="panel-heading" role="tab" id="heading-<?php echo $i ?>">
		<h4 class="panel-title">
			<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $i ?>" aria-expanded="false" aria-controls="collapse-<?php echo $i ?>" class="collapsed">
				<?php echo $row['title']; ?>
			</a>
		</h4>
	</div>
	<div id="collapse-<?php echo $i ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $i ?>">
		<div class="panel-body">
			<?php echo $row['content']; ?>
		</div>
	</div>
</div>
