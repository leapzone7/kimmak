<?php 
$title 	= $gallery_item['title'];
$alt 	= $gallery_item['alt'];
$thumb 	= $gallery_item['sizes']['medium'];
$full 	= $gallery_item['url'];
?>
<div class="cbp-item <?php echo $post->post_name; ?>">
    <a class="cbp-lightbox" data-title="<?php echo $title ?>" href="<?php echo $full; ?>">
        <img src="<?php echo $thumb ?>" alt="<?php echo $alt; ?>" width="100%"/>
    </a>
</div>