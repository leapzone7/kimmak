<li>
	<span><?php echo $file['title']; ?></span>
	<div class="download container-flex">
		<span class="pdf-icon"></span> 
		<?php if (is_user_logged_in()): ?>
			<?php if ( (isset($roles) && in_array($current_user_role, $roles) ) || $current_user_role == 'administrator' || $role == 'all'): ?>
				<a target="_blank" href="<?php echo $file['file']; ?>">[PDF download]</a>
			<?php else: ?>
				<a href="#" data-toggle="modal" data-target="#modal-access-denied">[PDF download]</a>
			<?php endif; ?>
		<?php else: ?>
			<a href="#" class="popup-login">[PDF download]</a>
		<?php endif; ?>
	</div>
</li>
